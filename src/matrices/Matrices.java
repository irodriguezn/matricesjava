/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

import java.util.Scanner;
import vector.Vector;

/**
 *
 * @author nacho
 */
public class Matrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Matriz m1, m2, m3,m4;
        m1= new Matriz(3,3);
        m2= new Matriz(3,3);
        m1.rellenaAleatorio(0,2);
        m2.rellena();
        m1.mostrar();
        m2.mostrar();
        m3=m1.producto(m2);
        m3.mostrar();
        m4=m2.producto(m1);
        m4.mostrar();
        /*m2=m1.adjunta();
        m2.mostrar();
        det=m1.determinante();
        System.out.println("determinante: " + det);
        m2=m2.dividir(det);
        m2.mostrar();*/
        //System.out.println("Determinante: " + m1.determinante());
        /*if (m1.esCuadrada()) {
            for (int i=0; i<m1.getFilas(); i++) {
                for (int j=0; j<m1.getColumnas(); j++) {
                    m2=m1.cofactor(i,j);
                    m2.mostrar();
                } 
            }
        }*/
        
        //m2=m1.transponer();
        //m2.mostrar();
        //System.out.println("Det 1: "+m1.determinante());
        //System.out.println("Det 2: "+m2.determinante());
    }
    
}
