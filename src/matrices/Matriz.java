/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

import java.util.Scanner;
import vector.Vector;

/**
 *
 * @author nacho
 */
public class Matriz {
    private double matriz[][];
    
    Matriz(int filas, int columnas) {
        matriz=new double[filas][columnas];
    }
    
    Matriz(int talla) {
        matriz=new double[talla][talla];
    }
    
    Matriz(double m[][]) {
        matriz=m;
    }
    
    public int getFilas() {
        return matriz.length;
    }
    
    public int getColumnas() {
        return matriz[0].length;
    }
    
    public boolean esCuadrada() {
        return matriz[0].length==matriz.length;
    }
    
    public double getPos(int x, int y) {
        return matriz[x][y];
    }
    
    public void rellenaFila(int fila, int f[]) {
        System.arraycopy(f, 0, matriz[fila], 0, matriz[0].length);
        
        /*for (int i=0; i<matriz[0].length; i++) {
        matriz[fila][i]=f[i];
        }*/
    }
    
    public void rellena() {
        Scanner sc = new Scanner(System.in);
        String numeros[]=new String[matriz.length];
        String linea;
        for (int i=0; i<matriz.length; i++) {
            System.out.print("Introduzca fila " + i + " : ");
            numeros[i]=sc.nextLine();
        }
        for (int i=0; i<matriz.length; i++) {
            linea=numeros[i];
            for (int j=0; j<matriz[0].length; j++) {
                matriz[i][j]=obtenerSiguiente(linea);
                linea=extraerNumero(linea);
            }
        }
    }
    
    static int obtenerSiguiente(String numeros) {
        int num=0;
        int pos=0;
        String numero;
        numeros=numeros+" ";
        pos=numeros.indexOf(" ");
        numero=numeros.substring(0, pos);
        num=Integer.parseInt(numero);
        return num;
    }
    static String extraerNumero(String numeros) {
        int pos=0;
        String aux;
        pos=numeros.indexOf(" ");
        aux=numeros.substring(pos+1, numeros.length());
        return aux;
    }
    
    public void rellenaAleatorio(int inicio, int fin) {
        for (int i=0; i<matriz.length; i++) {
            for (int j=0; j<matriz[0].length; j++) {
                matriz[i][j]=(int) (Math.random() * (fin-inicio+1) + inicio);
            }
        }
    }
    
    public void mostrar() {
        for (double[] fila : matriz) {
            for (double el : fila) {
                System.out.print(el + " ");
            }
            System.out.println();
        }
        System.out.println("");
    }
    
    public double[] getFila(int fila) {
        double vFila[]=new double[matriz.length];
        for (int i=0; i<matriz.length; i++) {
            vFila[i]=matriz[fila][i];
        }
        return vFila;
    }
    
    public double[] getColumna(int col) {
        double vCol[]=new double[matriz[0].length];
        for (int i=0; i<matriz[0].length; i++) {
            vCol[i]=matriz[i][col];
        }
        return vCol;
    }
    
    // Para que funcione correctamente la matriz que multiplica ha de tener
    // el mismo número de filas que columnas la multiplicada
    public Matriz producto(Matriz m) {
        double p[][]=new double[matriz.length][m.getColumnas()];
        for (int i=0; i<matriz.length; i++) {
            for (int j=0; j<m.getColumnas(); j++) {
                p[i][j]=productoV(getFila(i),m.getColumna(j));
            }
        }
        Matriz mat=new Matriz(p);
        return mat;
    }
    
    static double productoV(double[] v1, double[] v2) {
        double producto=0;
        for (int i=0; i<v1.length; i++) {
            producto+=(v1[i]*v2[i]);
        }
        return producto;
    }
    
    public Matriz transponer() {
        double m[][]=new double [matriz[0].length][matriz.length];
        for (int i=0; i<matriz.length; i++) {
            for (int j=0; j<matriz[0].length; j++) {
                m[j][i]=matriz[i][j];
            }
        }
        Matriz mat=new Matriz(m);
        return mat;
    }
    
    // Requisito previo para que funcione es que la matriz m sea cuadrada. 
    public Matriz cofactor(int posX, int posY) {
        double mAux[][]=new double[matriz.length-1][matriz[0].length-1];
        int fila=0;
        int columna=0;
        for (int i=0; i<matriz.length; i++) {
            if (posX!=i) {
                for (int j=0; j<matriz[0].length; j++) {
                    if (posY!=j) {
                        mAux[fila][columna]=matriz[i][j];
                        columna++;
                    }
                }
                fila++;
                columna=0;
            }
        }
        Matriz mat=new Matriz(mAux);
        return mat;
    }
    
    public Matriz adjunta() {
        double m[][]=new double [matriz.length][matriz.length];
        for (int i=0; i<matriz.length; i++) {
            for (int j=0; j<matriz.length; j++) {
                if ((i+j)%2==0) {
                    m[i][j]=cofactor(i,j).determinante();
                } else {
                    m[i][j]=-cofactor(i,j).determinante();
                }
            }
        }
        Matriz mat=new Matriz(m);
        return mat.transponer();
    }
    
    public Matriz inversa() {
        Matriz mat=adjunta();
        double det=determinante();
        return mat.dividir(det);
    }
    
    public Matriz dividir(double num) {
        double m[][]=new double [matriz.length][matriz[0].length];
        for (int i=0; i<matriz.length; i++) {
            for (int j=0; j<matriz.length; j++) {
                m[i][j]=matriz[i][j]/num;
            }
        }        
        Matriz mat=new Matriz(m);
        return mat;
    }
    
    // Para que funcione correctamente las matrices deben de ser cuadradas
    public double determinante() {
        if (matriz.length==1) {
            return matriz[0][0];
        } else if (matriz.length==2) {
            return matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];
        } else {
            int negativos=0;
            int positivos=0;
            for (int j=0; j<matriz.length; j++) {
                if ((j)%2==0) {
                    positivos+=matriz[0][j]*cofactor(0,j).determinante();
                } else {
                    negativos+=matriz[0][j]*cofactor(0,j).determinante();
                }
            }
            return positivos-negativos;
        }
        //int pos=matriz[0][0]*matriz[1][1]*matriz[2][2]+matriz[0][1]*matriz[1][2]*matriz[2][0]+matriz[1][0]*matriz[2][1]*matriz[0][2];
        //int neg=matriz[2][0]*matriz[1][1]*matriz[0][2]+matriz[1][0]*matriz[0][1]*matriz[2][2]+matriz[1][2]*matriz[2][1]*matriz[0][0];
        //return pos-neg;
    }
}
